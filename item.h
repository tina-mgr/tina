/*
 * tina - a personal information manager
 * Copyright (C) 2001  Matt Kraai
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#ifndef TINA_ITEM_H
#define TINA_ITEM_H

#include <sys/types.h>

struct item
{
  char *identifier;
  char *description;
  char **categories;
  size_t ncategories;
};

/* Create a new item.  */
struct item *item_new ();
/* Create a new item, and set its description to DESCRIPTION.  */
struct item *item_new_with_description (const char *description);
/* Create a copy of IT.  */
struct item *item_clone (struct item *it);
/* Delete IT.  */
void item_delete (struct item *it);

/* Set the identifier of IT to IDENTIFIER.  */
void item_identifier_set (struct item *it, const char *identifier);
/* Set the description of IT to DESCRIPTION.  */
void item_description_set (struct item *it, const char *description);
/* Add IT to CATEGORY.  */
void item_category_add (struct item *it, const char *category);
/* Remove IT from CATEGORY.  */
void item_category_remove (struct item *it, const char *category);
/* Return nonzero iff IT is a member of CATEGORY.  */
int item_category_member_p (struct item *it, const char *category);

#endif /* TINA_ITEM_H */
