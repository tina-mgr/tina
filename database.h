/*
 * tina - a personal information manager
 * Copyright (C) 2001  Matt Kraai
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#ifndef TINA_DATABASE_H
#define TINA_DATABASE_H

#include <sys/types.h>

#include "item.h"

struct database
{
  char *path;

  struct item **items;
  size_t nitems;

  int readonly:1;
};

/* Create a database whose backing store is PATH.  */
struct database *database_new_with_path (const char *path);
/* Write DB to its backing store.  */
void database_sync (struct database *db);
/* Write DB to its backing store and delete.  */
void database_delete (struct database *db);

/* Add IT to DB at POS.  */
void database_item_add (struct database *db, struct item *it, int pos);
/* Remove the item at POS from DB.  */
void database_item_remove (struct database *db, int pos);
/* Return the position of IT in DB->items, or DB->nitems if it is not
   present.  */
int database_item_index (struct database *db, struct item *it);

#endif /* TINA_DATABASE_H */
