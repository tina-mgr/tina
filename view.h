/*
 * tina - a personal information manager
 * Copyright (C) 2001  Matt Kraai
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#ifndef TINA_VIEW_H
#define TINA_VIEW_H

#include "item.h"
#include "selection.h"

struct view
{
  struct view *prev;
  struct selection *s;

  int selected;
  char *search_pattern;
};

/* Create a new view, and set its selection to S.  */
struct view *view_new_with_selection (struct selection *s);
/* Delete V.  */
void view_delete (struct view *v);

/* Show V.  */
void view_show (struct view *v);

#endif /* TINA_VIEW_H */
