/*
 * tina - a personal information manager
 * Copyright (C) 2001  Matt Kraai
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#ifndef TINA_SELECTION_H
#define TINA_SELECTION_H

#include <sys/types.h>

#include "database.h"
#include "item.h"

struct selection
{
  struct database *db;
  char *category;

  struct item **items;
  size_t nitems;
};

/* Create a new selection.  */
struct selection *selection_new_with_database (struct database *db);
/* Delete S.  */
void selection_delete (struct selection *s);

/* Set the category of S to CATEGORY.  */
void selection_category_set (struct selection *s, const char *category);
/* Refresh S to reflect changes to the database.  */
void selection_refresh (struct selection *s);

/* Add IT to S at POS.  */
void selection_item_add (struct selection *s, struct item *it, int pos);
/* Remove the item at POS from S.  */
void selection_item_remove (struct selection *s, int pos);
/* Return the position of IT in S->items, or S->nitems if it is not present.  */
int selection_item_index (struct selection *s, struct item *it);

#endif /* TINA_SELECTION_H */
