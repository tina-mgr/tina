/*
 * tina - a personal information manager
 * Copyright (C) 2002  Matt Kraai
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#ifndef TINA_CURSLIB_H
#define TINA_CURSLIB_H

enum
{
  color_default = 1,
  color_selected,
  color_status
};

/* Turn on highlighting for the help or mode line.  */
void highlight (void);
/* Turn off highlighting for the help or mode line.  */
void lowlight (void);
/* Pad with spaces until the end of line.  */
void pad_to_eol (void);

/* Display PROMPT and return the user's response.  Use VALUE as a default if it
   is not NULL.  */
char *inquire (const char *prompt, const char *value);

#define CLEARLINE(LINE) do { move ((LINE), 0); clrtoeol (); } while (0)
#define CONTROL(X) ((X) & ~0x40)

#endif /* TINA_CURSLIB_H */
