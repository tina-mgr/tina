/*
 * tina - a personal information manager
 * Copyright (C) 2001  Matt Kraai
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#ifndef TINA_MEMORY_H
#define TINA_MEMORY_H

#include <sys/types.h>

/* Reallocate PTR to be SIZE bytes.  */
void *xrealloc (void *ptr, size_t size);
/* Allocate a pointer to SIZE bytes.  */
void *xmalloc (size_t size);
/* Allocate a zeroed array of NMEMB SIZE-byte elements.  */
void *xcalloc (size_t nmemb, size_t size);
/* Allocate a copy of S.  */
char *xstrdup (const char *s);
/* Allocate a copy of the first N characters of S.  */
char *xstrndup (const char *s, size_t n);

#endif /* TINA_MEMORY_H */
